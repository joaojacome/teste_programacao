<?php
	// configuração de inicio e fim do periodo noturno
	$inicioPeriodoNorturno = '22:00';
	$fimPeriodoNorturno   = '07:00';
	
	// empregados e jornadas de trabalho
	$empregados = array(
		
		'0' => array(
			'nome'        => 'Bernice Lyons',
			'inicio_jornada' => '15:15',
			'fim_jornada'   => '23:45'
		),
		
		'1' => array(
			'nome'        => 'Gregg Santos',
			'inicio_jornada' => '10:00',
			'fim_jornada'   => '22:00'
		),
		
		'2' => array(
			'nome'        => 'Bennie Montgomery',
			'inicio_jornada' => '22:30',
			'fim_jornada'   => '08:00'
		),
		
		'3' => array(
			'nome'        => 'Nelson Austin',
			'inicio_jornada' => '20:00',
			'fim_jornada'   => '10:00'
		),
		
		'4' => array(
			'nome'        => 'Garrett Sims',
			'inicio_jornada' => '09:00',
			'fim_jornada'   => '17:00'
		),
		
		'5' => array(
			'nome'        => 'Joanna Pratt',
			'inicio_jornada' => '23:00',
			'fim_jornada'   => '06:00'
		)
		
	);
	

?>