<table>
<thead>
<tr>
<th>Nome</th>
<th>Hora de Início</th>
<th>Hora de Término</th>
<th>Qtd. de horas trabalhadas</th>
<th>Qtd. de horas diurnas</th>
<th>Qtd. de horas noturnas</th>
</tr>
</thead>
<tbody>
<?php
require_once("input.php");
function floatToTime($time) {
    return sprintf('%02d:%02d', (int) $time, fmod($time, 1) * 60);
}
sscanf($inicioPeriodoNorturno,"%d:%d",$inicio_noturno,$inicio_noturno_min);
sscanf($fimPeriodoNorturno,"%d:%d",$fim_noturno,$fim_noturno_min);
$total_noturno = 24-$inicio_noturno+$fim_noturno;
$intervalo_noturno = range($inicio_noturno, $inicio_noturno+$total_noturno, .25);

foreach ($empregados as $empregado) {
    sscanf($empregado["inicio_jornada"],"%d:%d", $inicio_hr, $inicio_min);
    sscanf($empregado["fim_jornada"], "%d:%d", $fim_hr, $fim_min);
    //transformar os dois dias em um intervalo unico
    $min_inicial = $inicio_min%15==0?($inicio_min/15)*.25:0;
    $hora_inicial = $inicio_hr+$min_inicial;
    $min_final = $fim_min%15==0?($fim_min/15)*.25:0;
    $hora_final = $fim_hr+$min_final;
    if ($inicio_hr > $fim_hr) {
        $hora_final += 24;
    }
    $total_horas = $hora_final-$hora_inicial;
    $intervalo = range($hora_inicial, $hora_final, .25);
    //pegar a intercecao dos dois arrays, pra ver quais minutos batem
    $horas_noturno = array_values(array_intersect($intervalo, $intervalo_noturno));
    $total_noturno = 0;
    if (count($horas_noturno) > 0) {
        //somar as pontas do array de intersecao
        $total_noturno = $horas_noturno[count($horas_noturno)-1] - $horas_noturno[0];
    }
    $total_hr_diurnas = $total_horas-$total_noturno;
    echo "<tr>";
    echo "<td>", $empregado["nome"], "</td>";
    echo "<td>", $empregado["inicio_jornada"], "</td>";
    echo "<td>", $empregado["fim_jornada"], "</td>";
    echo "<td>", floatToTime($total_horas), "</td>";
    echo "<td>", floatToTime($total_hr_diurnas), "</td>";
    echo "<td>", floatToTime($total_noturno), "</td>";
    echo "</tr>";
}
?>
</tbody>
</table>