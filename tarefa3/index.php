<?php

$cartoes = json_decode(file_get_contents("cartoes.json"),true);

shuffle($cartoes);

$origens = $destinos = [];
$fnum = count($cartoes);
$novoCartoes = [];

//dividir origens e destinos em arrays que possam ser comparados
for ($i = 0; $i < count($cartoes); $i++) {
	$origens[] = $origem = $cartoes[$i]["origem"]["cidade"];
	$destinos[] = $cartoes[$i]["destino"]["cidade"];
	//atribuir o cartao atual a um novo array, com o index sendo a origem
	$novoCartoes[$origem] = $cartoes[$i];
}
//comparar os arrays de origens e destinos, para determinar a origem da primeira viagem
foreach ($origens as $ori) {
	if (!in_array($ori,$destinos)) {
		$origem = $ori;
	}
}
//exibir os passos
echo "<ul>";
while (isset($novoCartoes[$origem])) {
	$c = $novoCartoes[$origem];
	if ($c["destino"]["cod_destino"]) {
	}
	if ($c["origem"]["local"]) {
		$c["origem"]["local"] = " (".$c["origem"]["local"].")";
	}
	if ($c["destino"]["local"]) {
		$c["destino"]["local"] = " (".$c["destino"]["local"];
		if($c["destino"]["cod_destino"]) {
			$c["destino"]["local"] .= " ".$c["destino"]["cod_destino"];
		}
		$c["destino"]["local"] .= ")";
	}
	echo "<li>\n";
	echo "<b>Tipo de transporte:</b> ", $c["transporte"]["tipo"], "<br/>\n";
	echo "<b>Origem:</b> ", $c["origem"]["cidade"], $c["origem"]["local"], "<br/>\n";
	echo "<b>Destino:</b> ", $c["destino"]["cidade"], $c["destino"]["local"], "<br/>\n";
	if ($c["transporte"]["codigo"]) {
		echo "<b>Código do ", mb_strtolower($c["transporte"]["tipo"]=="Avião"?"vôo":$c["transporte"]["tipo"]), ":</b> ", $c["transporte"]["codigo"], "<br/>\n";
	}
	if ($c["transporte"]["assento"]) {
		echo "<b>Assento:</b> ", $c["transporte"]["assento"], "<br/>\n";
	} else {
		echo "<b>Sem definição de assento.</b><br/>";
	}
	if (isset($c["transporte"]["extra"])) {
		echo "<b>Informações:</b> ", implode(" ",$c["transporte"]["extra"]), "<br/>\n";

	}
	echo "<hr/>";
	echo "</li>\n";
	//echo $c["origem"]["cidade"]." - ".$c["destino"]["cidade"]."\n";
	$origem = $c["destino"]["cidade"];
}