<ul>
<?php

for ($i=1; $i<=100; $i++) {
	$condition1 = $i % 3 == 0;
	$condition2 = $i % 5 == 0;
	$cn = ($condition1 && $condition2) ? "foobar" : ( $condition1 ? "foo" : ($condition2 ? "bar" : $i));
	echo "<li>", $cn, "</li>\n";
}

?>
</ul>